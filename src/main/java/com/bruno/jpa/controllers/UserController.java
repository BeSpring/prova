package com.bruno.jpa.controllers;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bruno.jpa.domain.User;
import com.bruno.jpa.services.UserService;

@RestController
@RequestMapping("/user")
public class UserController {
	private UserService userService;

	@Autowired
	public void setBookService(UserService userService) {
		this.userService = userService;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping("/list")
	public  List<User> listBooks() {
		
		return  (List<User>) userService.listAll();
	}
//	@RequestMapping("/show")
//	public  User listById(@PathParam(value = "id")  Integer id) {
//		
//		return  userService.getById(id);
//	}
	@RequestMapping("/show/{id}")
	public  User listById(@PathVariable  Integer id) {
		
		return  userService.getById(id);
	}
	

}
