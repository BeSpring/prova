package com.bruno.jpa.services;

import java.util.List;

public interface CRUDService<T> {
	List<?> listAll();

	T getById(Integer id);


}
