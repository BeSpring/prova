package com.bruno.jpa.services.jpaservices;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Service;

import com.bruno.jpa.domain.User;
import com.bruno.jpa.services.UserService;

@Service
public class UserServiceImpl extends AbstractJpaDaoService implements UserService {
	@Override
	public User getById(Integer id) {
		EntityManager em = emf.createEntityManager();
		return em.find(User.class, id);
	}

	@Override
	public List<User> listAll() {
		EntityManager em = emf.createEntityManager();
		return em.createQuery("from User", User.class).getResultList();
	}

}